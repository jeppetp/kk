var width = window.innerWidth
var height = window.innerHeight

// euclidean distance
let dist = ((p1, p2) => sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

function windowResized() {
  width = windowWidth
  height = windowHeight
  resizeCanvas(width, height)
  if (width > height) {
    widthoffset = (width - height) / 2
  }
  else {
    heightoffset = (height - width) / 2
  }
}

var widthoffset = 0
var heightoffset = 0

function setup() {
  if (width > height) {
    widthoffset = (width - height) / 2
  }
  else {
    heightoffset = (height - width) / 2
  }
  createCanvas(windowWidth, windowHeight)
  colorMode(HSB, 255)
  frameRate(30)
  textSize(10);
  noStroke();

  // create sliders
  timedelaySlider = createSlider(0, 11, 6.5, 0);
  timedelaySlider.position(10, 10);
  timedelaySlider.style('width', '200px')

  chaosSlider = createSlider(0, 100, 0, 0);
  chaosSlider.position(10, 30);
  chaosSlider.style('width', '200px')

  coneSlider = createSlider(1, 100, 15, 0);
  coneSlider.position(10, 50);
  coneSlider.style('width', '200px')

  timeSlider = createSlider(0.0005, 0.1, 0.01, 0);
  timeSlider.position(10, 70);
  timeSlider.style('width', '200px')

  colortimeSlider = createSlider(0, 2000, 1000, 0);
  colortimeSlider.position(10, 90);
  colortimeSlider.style('width', '200px')

  sizeSlider = createSlider(1, 200, 15, 0);
  sizeSlider.position(10, 110);
  sizeSlider.style('width', '200px')

  alphaSlider = createSlider(0, 255, 255, 1);
  alphaSlider.position(10, 130);
  alphaSlider.style('width', '200px')

  lsel = createSelect();
  lsel.style('width', '95px')
  lsel.style('height', '10px')
  lsel.position(10, 160);
  lsel.option('1');
  lsel.option('2');
  lsel.option('4');
  lsel.option('8');
  lsel.option('16');
  lsel.option('32');
  lsel.option('64');
  lsel.option('128');
  lsel.option('256');
  lsel.selected('64')

  ksel = createSelect();
  ksel.style('width', '95px')
  ksel.style('height', '10px')
  ksel.position(115, 160);
  ksel.option(1);
  ksel.option(2);
  ksel.option('4');
  ksel.option('8');
  ksel.option('16');
  ksel.option('32');
  ksel.option('64');
  ksel.option('128');
  ksel.option('256');
  ksel.selected('16')

  blendsel = createSelect();
  blendsel.style('width', '200px')
  blendsel.style('height', '10px')
  blendsel.position(10, 190);
  blendsel.option(BLEND);
  blendsel.option(ADD);
  blendsel.option(LIGHTEST);
  blendsel.option(DIFFERENCE);
  blendsel.option(EXCLUSION);
  blendsel.option(SCREEN);
  blendsel.option(OVERLAY);
  blendsel.option(HARD_LIGHT);
  blendsel.option(SOFT_LIGHT);
  blendsel.option(DODGE);
  blendsel.selected(DIFFERENCE)


}

function noiseMap(y, t, scale, k){
  var angle = map(y, 0, k, 0, TWO_PI)
  return [noise(scale*cos(angle)+1.11612,scale*sin(angle)+5.12315,t) * min(width, height) + widthoffset,noise(scale*cos(angle)+123.11413, scale*sin(angle) + 1263.5611, t+13.1241)*min(height,width) + heightoffset]
  // return [noise(scale*y+1.11612,scale*y+5.12315,t) * width,noise(scale*y+123.11413, scale*y + 1263.5611, t+13.1241)*height]

}

let fg = [190, 100, 100]
let bg = [0,0 , 0]
// let blobcolor = [190, 180, 100]
// var blob = [0, 0]
// var blobdir = Math.PI
// var blobnoise = 1
// var blobturnspeed = 0.10
// var blobspeed = 0.5
var t = 0
function draw() {
  const alpha = alphaSlider.value()
  const l = int(lsel.value())+1
  const k = ksel.value()
  const timedelay = timedelaySlider.value()
  const chaos = chaosSlider.value()
  const size = sizeSlider.value()
  const time = timeSlider.value()
  const colortime = colortimeSlider.value()
  const cone = coneSlider.value()
  blendMode(REPLACE)
  background(...bg)
  blendMode(blendsel.value())
  for (i = 0; i < l; i++){
    for (j = 0; j < k; j++){
      // strokeWeight(1)
      // stroke(0,0,0)
      noStroke()
      // fill(255,0,255, 50)
      fill((map(j, 0,k,0,255) + map(i, 0, l, 0, 255) + colortime*t)%255, 255, 255, alpha)
      circle(...noiseMap(j, t + i/(2**timedelay), map(i, 0, l-1, cone/100, chaos/100), k),map(i,0,l,0,size))
    }
  }
  t+= time
  textSize(12);
  fill(0,0,255)
  text('time delay', 215, 24);
  text('front width', 215, 44);
  text('back width', 215, 64);
  text('speed', 215, 84);
  text('color speed', 215, 104);
  text('size', 215, 124);
  text('alpha', 215, 144);
  // t+=noise(t*10)*0.01
}
